# CardioRisk-TextMining
Creating Clinical Value Out Of Text: A Text-Mining Manual Applied to Cardiovascular Risk Prediction

Aim of this project is to develop and evaluate a text-mining pipeline to show the process of medical text mining. We illustrate evaluation of text mining relevance using the example of text mining chest x-ray radiology reports for cardiovascular risk prediction. 
